package com.afs.tdd.dto;

import lombok.Getter;

public enum DirectionTypeEnum {
    N("N"),
    S("S"),
    E("E"),
    W("W");

    @Getter
    private final String direction;

    DirectionTypeEnum(String direction) {
        this.direction = direction;
    }
}
