package com.afs.tdd.dto;

import lombok.Getter;

public enum CommandTypeEnum {

    MOVE("M"),
    TURN_LEFT("L"),
    TURN_RIGHT("R");

    @Getter
    private final String command;

    CommandTypeEnum(String command) {
        this.command = command;
    }
}
