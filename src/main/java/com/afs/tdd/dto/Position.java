package com.afs.tdd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@Data
@SuperBuilder(toBuilder = true)
public class Position {

    private Integer x;
    private Integer y;

    private DirectionTypeEnum direction;

}
