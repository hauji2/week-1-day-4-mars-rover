package com.afs.tdd;

import com.afs.tdd.dto.CommandTypeEnum;
import com.afs.tdd.dto.DirectionTypeEnum;
import com.afs.tdd.dto.Position;

import java.util.List;

public class ExecuteCommands {

    public String executeCommands(String commands, Position position) {
        Position newPosition = position.toBuilder().build();
        List<String> commandsList = commands.chars()
                .mapToObj(c -> String.valueOf((char) c))
                .toList();

        // use for loop to allow position to be updated instead of using for-each
        for (String command : commandsList) {
            newPosition = executeCommand(newPosition, command);
        }

        return convertPositionToStringOutput(newPosition);
    }

    public Position move(Position position) {
        Position newPosition = position.toBuilder().build();

        switch (position.getDirection()) {
            case N -> newPosition.setY(position.getY() + 1);
            case E -> newPosition.setX(position.getX() + 1);
            case S -> newPosition.setY(position.getY() - 1);
            case W -> newPosition.setX(position.getX() - 1);
        }
        return newPosition;
    }

    public Position turnLeft(Position position) {
        Position newPosition = position.toBuilder().build();

        switch (position.getDirection()) {
            case N -> newPosition.setDirection(DirectionTypeEnum.W);
            case E -> newPosition.setDirection(DirectionTypeEnum.N);
            case S -> newPosition.setDirection(DirectionTypeEnum.E);
            case W -> newPosition.setDirection(DirectionTypeEnum.S);
        }
        return newPosition;
    }

    public Position turnRight(Position position) {
        Position newPosition = position.toBuilder().build();

        switch (position.getDirection()) {
            case N -> newPosition.setDirection(DirectionTypeEnum.E);
            case E -> newPosition.setDirection(DirectionTypeEnum.S);
            case S -> newPosition.setDirection(DirectionTypeEnum.W);
            case W -> newPosition.setDirection(DirectionTypeEnum.N);
        }
        return newPosition;
    }

    private Position executeCommand(Position position, String command) {
        if (CommandTypeEnum.MOVE.getCommand().equals(command)) {
            return move(position);
        } else if (CommandTypeEnum.TURN_LEFT.getCommand().equals(command)) {
            return turnLeft(position);
        } else if (CommandTypeEnum.TURN_RIGHT.getCommand().equals(command)) {
            return turnRight(position);
        }
        return position;
    }

    private String convertPositionToStringOutput(Position position) {
        return position.getX() + " " + position.getY() + " " + position.getDirection().getDirection();
    }
}
