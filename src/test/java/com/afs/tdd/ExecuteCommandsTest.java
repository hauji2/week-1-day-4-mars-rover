package com.afs.tdd;

import com.afs.tdd.dto.DirectionTypeEnum;
import com.afs.tdd.dto.Position;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExecuteCommandsTest {

    @Test
    void should_run_move_command_when_executeCommands_given_move_command() {
        Position position = new Position(0, 0, DirectionTypeEnum.N);
        String commands = "M";
        String expected = "0 1 N";

        String actual = new ExecuteCommands().executeCommands(commands, position);

        assertEquals(expected, actual);
    }

    @Test
    void should_run_move_and_turn_left_command_when_executeCommands_given_move_and_turn_left_command() {
        Position position = new Position(0, 0, DirectionTypeEnum.N);
        String commands = "ML";
        String expected = "0 1 W";

        String actual = new ExecuteCommands().executeCommands(commands, position);

        assertEquals(expected, actual);
    }

    @Test
    void should_run_move_and_turn_left_and_turn_right_command_when_executeCommands_given_move_and_turn_left_and_turn_right_command() {
        Position position = new Position(0, 0, DirectionTypeEnum.N);
        String commands = "MLR";
        String expected = "0 1 N";

        String actual = new ExecuteCommands().executeCommands(commands, position);

        assertEquals(expected, actual);
    }


    @Test
    void should_y_increase_one_when_move_given_facing_north() {
        Position position = new Position(0,0, DirectionTypeEnum.N);
        Position expected = new Position(0,1, DirectionTypeEnum.N);

        Position actual = new ExecuteCommands().move(position);

        assertEquals(expected, actual);

    }

    @Test
    void should_x_increase_one_when_move_given_facing_east() {
        Position position = new Position(0, 0, DirectionTypeEnum.E);
        Position expected = new Position(1, 0, DirectionTypeEnum.E);

        Position actual = new ExecuteCommands().move(position);

        assertEquals(expected, actual);
    }

    @Test
    void should_y_decrease_one_when_move_given_facing_south() {
        Position position = new Position(0, 0, DirectionTypeEnum.S);
        Position expected = new Position(0, -1, DirectionTypeEnum.S);

        Position actual = new ExecuteCommands().move(position);

        assertEquals(expected, actual);
    }

    @Test
    void should_x_decrease_one_when_move_given_facing_west() {
        Position position = new Position(0, 0, DirectionTypeEnum.W);
        Position expected = new Position(-1, 0, DirectionTypeEnum.W);

        Position actual = new ExecuteCommands().move(position);

        assertEquals(expected, actual);
    }

    @Test
    void should_direction_be_east_when_turn_right_given_facing_north() {
        Position position = new Position(0, 0, DirectionTypeEnum.N);
        Position expected = new Position(0, 0, DirectionTypeEnum.E);

        Position actual = new ExecuteCommands().turnRight(position);

        assertEquals(expected, actual);
    }

    @Test
    void should_direction_be_south_when_turn_right_given_facing_east() {
        Position position = new Position(0, 0, DirectionTypeEnum.E);
        Position expected = new Position(0, 0, DirectionTypeEnum.S);

        Position actual = new ExecuteCommands().turnRight(position);

        assertEquals(expected, actual);
    }

    @Test
    void should_direction_be_west_when_turn_right_given_facing_south() {
        Position position = new Position(0, 0, DirectionTypeEnum.S);
        Position expected = new Position(0, 0, DirectionTypeEnum.W);

        Position actual = new ExecuteCommands().turnRight(position);

        assertEquals(expected, actual);
    }

    @Test
    void should_direction_be_north_when_turn_right_given_facing_west() {
        Position position = new Position(0, 0, DirectionTypeEnum.W);
        Position expected = new Position(0, 0, DirectionTypeEnum.N);

        Position actual = new ExecuteCommands().turnRight(position);

        assertEquals(expected, actual);
    }

    @Test
    void should_direction_be_west_when_turn_left_given_facing_north() {
        Position position = new Position(0, 0, DirectionTypeEnum.N);
        Position expected = new Position(0, 0, DirectionTypeEnum.W);

        Position actual = new ExecuteCommands().turnLeft(position);

        assertEquals(expected, actual);
    }

    @Test
    void should_direction_be_south_when_turn_left_given_facing_west() {
        Position position = new Position(0, 0, DirectionTypeEnum.W);
        Position expected = new Position(0, 0, DirectionTypeEnum.S);

        Position actual = new ExecuteCommands().turnLeft(position);

        assertEquals(expected, actual);
    }

    @Test
    void should_direction_be_east_when_turn_left_given_facing_south() {
        Position position = new Position(0, 0, DirectionTypeEnum.S);
        Position expected = new Position(0, 0, DirectionTypeEnum.E);

        Position actual = new ExecuteCommands().turnLeft(position);

        assertEquals(expected, actual);
    }

    @Test
    void should_direction_be_north_when_turn_left_given_facing_east() {
        Position position = new Position(0, 0, DirectionTypeEnum.E);
        Position expected = new Position(0, 0, DirectionTypeEnum.N);

        Position actual = new ExecuteCommands().turnLeft(position);

        assertEquals(expected, actual);
    }
}
